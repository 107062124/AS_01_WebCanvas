var canvas = document.getElementById('Canvas');
var ctx = canvas.getContext('2d');
var brushsize_slider = document.getElementById('brushsize_slider');
var brushsize_display = document.getElementById('brushsize_display');
var fontsize_slider = document.getElementById('fontsize_slider');
var fontsize_display = document.getElementById('fontsize_display');
var font = document.getElementById('font');
var mode = "";
// record last textbox's pos (used to deal with click other place and fill text on canvas)
var last_textbox_pos = {x : 0, y : 0};

// for circle
var center = {x : 0, y : 0};
var radius;
// record the shape's start point
var shape_start_point = {x : 0, y : 0};
// record the img before drawing the shape
var img_before_shape = new Image();

// undo and redo array
var step_array = new Array();
var step = -1; // total how many step / now on which step

// current img
var current_img = new Image();

var mousemoved;

canvas.addEventListener("mousedown", start_draw);
canvas.addEventListener("mouseup", function() {

	canvas.removeEventListener("mousemove", mouseMove, false);

	if (mousemoved && mode != "text"){
		push_array();
		current_img.src = step_array[step];
	}

}, false);

function init(){
	var w = window.innerWidth * 0.8;
	var w_1 = window.innerWidth * 0.15;
	var h = w_1 * 3.3;

	// set canvas height, width
	canvas.setAttribute("height", h + 'px');
	canvas.setAttribute("width", w + 'px');
	canvas.style.visibility = "visible";

	// set toolbox height, width
	var toolbox = document.getElementById('toolbox');
	toolbox.style.height = h + 'px';
	toolbox.style.width = w_1 + 'px';
	toolbox.style.visibility = "visible";

	// default font
	ctx.font = "10px Arial";

	ctx.lineCap = "round";
	ctx.lineJoin = "round";

	push_array();

	current_img.src = step_array[step];

	for (var i = 0; i < 3; i++){
		document.getElementsByClassName('row-1')[i].style.height = w_1*0.28 + 'px';
		document.getElementsByClassName('row-1')[i].style.width = w_1*0.28 + 'px';
		document.getElementsByClassName('row-2')[i].style.height = w_1*0.28 + 'px';
		document.getElementsByClassName('row-2')[i].style.width = w_1*0.28 + 'px';
		document.getElementsByClassName('row-3')[i].style.height = w_1*0.28 + 'px';
		document.getElementsByClassName('row-3')[i].style.width = w_1*0.28 + 'px';
	}

	for (var j = 0; j < 2; j++){
		document.getElementsByClassName('row-4')[j].style.height = w_1*0.28 + 'px';
		document.getElementsByClassName('row-4')[j].style.width = w_1*0.28 + 'px';
	}

	document.getElementById('toolbox_title').style.fontSize = w_1*0.15 + 'px';
}

function resize(){
	var w = window.innerWidth * 0.8;
	var w_1 = window.innerWidth * 0.15;
	var h = w_1 * 3.3;

	// set canvas height, width
	canvas.setAttribute("height", h + 'px');
	canvas.setAttribute("width", w + 'px');
	canvas.style.visibility = "visible";

	// set toolbox height, width
	var toolbox = document.getElementById('toolbox');
	toolbox.style.height = h + 'px';
	toolbox.style.width = w_1 + 'px';
	toolbox.style.visibility = "visible";

	ctx.lineWidth = brushsize_slider.value;
	ctx.font = fontsize_slider.value + "px " + font.options[font.selectedIndex].value;

	ctx.lineCap = "round";
	ctx.lineJoin = "round";
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	ctx.drawImage(current_img, 0, 0);

	for (var i = 0; i < 3; i++){
		document.getElementsByClassName('row-1')[i].style.height = w_1*0.28 + 'px';
		document.getElementsByClassName('row-1')[i].style.width = w_1*0.28 + 'px';
		document.getElementsByClassName('row-2')[i].style.height = w_1*0.28 + 'px';
		document.getElementsByClassName('row-2')[i].style.width = w_1*0.28 + 'px';
		document.getElementsByClassName('row-3')[i].style.height = w_1*0.28 + 'px';
		document.getElementsByClassName('row-3')[i].style.width = w_1*0.28 + 'px';
	}

	for (var j = 0; j < 2; j++){
		document.getElementsByClassName('row-4')[j].style.height = w_1*0.28 + 'px';
		document.getElementsByClassName('row-4')[j].style.width = w_1*0.28 + 'px';
	}

	document.getElementById('toolbox_title').style.fontSize = w_1*0.15 + 'px';
}

// change the display and the line width of the brushsize
brushsize_display.innerHTML = brushsize_slider.value;
brushsize_slider.oninput = function(){
	brushsize_display.innerHTML = this.value;
	ctx.lineWidth = this.value;
};

// change the display and the fontsize
fontsize_display.innerHTML = fontsize_slider.value;
fontsize_slider.oninput = function (){
	fontsize_display.innerHTML = this.value;
	ctx.font = this.value + "px " + font.options[font.selectedIndex].value;
	// console.log(ctx.font);
};

// change font style
font.onchange = function (){
	ctx.font = fontsize_slider.value + "px " + this.options[this.selectedIndex].value;
	// console.log(ctx.font);
};

function getMousePos(canvas, evt){
	var rect = canvas.getBoundingClientRect();
	return {
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};
}

// main function drawing
function mouseMove(evt){
	mousemoved = true;
	var mouse_pos = getMousePos(canvas, evt);
	// console.log(mouse_pos.x, mouse_pos.y);

	if (mode == "pencil" || mode == "eraser"){
		ctx.lineTo(mouse_pos.x, mouse_pos.y);
		ctx.stroke();		
	}
	else if (mode == "circle"){
		radius = (Math.sqrt((mouse_pos.x - shape_start_point.x)*(mouse_pos.x - shape_start_point.x) + (mouse_pos.y - shape_start_point.y)*(mouse_pos.y - shape_start_point.y))) / 2;
		center.x = (mouse_pos.x + shape_start_point.x) / 2;
		center.y = (mouse_pos.y + shape_start_point.y) / 2;

		// before draw a new shape, go back to the step before drawing shape
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.beginPath();
		ctx.drawImage(img_before_shape, 0, 0);
		ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI);
		ctx.stroke();
	}
	else if (mode == "rectangle"){
		// rect_w = Math.abs(mouse_pos.x - shape_start_point.x);
		// rect_h = Math.abs(mouse_pos.y - shape_start_point.y);
		// before draw a new shape, go back to the step before drawing shape
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.beginPath();
		ctx.drawImage(img_before_shape, 0, 0);
		ctx.rect(shape_start_point.x, shape_start_point.y, mouse_pos.x - shape_start_point.x, mouse_pos.y - shape_start_point.y);
		ctx.stroke();
	}
	else if (mode == "triangle"){
		// before draw a new shape, go back to the step before drawing shape
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.beginPath();
		ctx.drawImage(img_before_shape, 0, 0);

		// triangle's three point, p1, p2, p3 => left, right, up
		var p1 = {x : 0, y : 0};
		var p2 = {x : 0, y : 0};
		var p3 = {x : 0, y : 0};

		// 4 situation (mouse位置分別在四個象限)
		// 右下
		if (mouse_pos.x >= shape_start_point.x && mouse_pos.y >= shape_start_point.y){
			p1.x = shape_start_point.x;
			p1.y = mouse_pos.y;
			p2.x = mouse_pos.x;
			p2.y = mouse_pos.y;
			p3.x = (p1.x + p2.x) / 2;
			p3.y = shape_start_point.y;
		}
		// 左下
		else if (mouse_pos.x <= shape_start_point.x && mouse_pos.y >= shape_start_point.y){
			p1.x = mouse_pos.x;
			p1.y = mouse_pos.y;
			p2.x = shape_start_point.x;
			p2.y = mouse_pos.y;
			p3.x = (p1.x + p2.x) / 2;
			p3.y = shape_start_point.y;
		}
		// 右上
		else if (mouse_pos.x >= shape_start_point.x && mouse_pos.y <= shape_start_point.y){
			p1.x = shape_start_point.x;
			p1.y = shape_start_point.y;
			p2.x = mouse_pos.x;
			p2.y = shape_start_point.y;
			p3.x = (p1.x + p2.x) / 2;
			p3.y = mouse_pos.y;
		}
		// 左上
		else {
			p1.x = mouse_pos.x;
			p1.y = shape_start_point.y;
			p2.x = shape_start_point.x;
			p2.y = shape_start_point.y;
			p3.x = (p1.x + p2.x) / 2;
			p3.y = mouse_pos.y;
		}

		// first line
		ctx.moveTo(p1.x, p1.y);
		ctx.lineTo(p2.x, p2.y);
		// second line
		ctx.lineTo(p3.x, p3.y);
		// third line
		ctx.closePath();
		ctx.stroke();
	}
}

function start_draw(evt){
	mousemoved = false;
	var mouse_pos = getMousePos(canvas, evt);
	var textbox;

	// if other textbox exists, draw the other textbox's value and remove other textbox
	if ((textbox = document.getElementById('textbox')) != null){
		if (textbox.value != ""){
			ctx.fillText(textbox.value, last_textbox_pos.x, last_textbox_pos.y);
			push_array();
			current_img.src = step_array[step];
		}
		document.body.removeChild(textbox);
	}

	// console.log(mouse_pos.x, mouse_pos.y);
	switch (mode){
		case "pencil" :
			ctx.globalCompositeOperation = "source-over";
			ctx.beginPath();
			ctx.moveTo(mouse_pos.x, mouse_pos.y);
			evt.preventDefault();
			canvas.addEventListener("mousemove", mouseMove, false);
			break;

		case "eraser" :
			ctx.globalCompositeOperation = "destination-out";
			ctx.beginPath();
			ctx.moveTo(mouse_pos.x, mouse_pos.y);
			evt.preventDefault();
			canvas.addEventListener("mousemove", mouseMove, false);
			break;

		case "text" :
			// create textbox
			textbox = document.createElement("INPUT");
			textbox.setAttribute("type", "text");
			textbox.setAttribute("id", "textbox");
			textbox.style.position = "absolute";
			textbox.style.left = evt.clientX + 'px';
			textbox.style.top = evt.clientY + 'px';
			document.body.appendChild(textbox);
			last_textbox_pos.x = mouse_pos.x;
			last_textbox_pos.y = mouse_pos.y;

			// typing
			textbox.addEventListener("keydown", function (k){
				if (k.key == "Enter"){
					if (textbox.value != ""){
						ctx.fillText(textbox.value, mouse_pos.x, mouse_pos.y);
						push_array();
						current_img.src = step_array[step];	
					}
					textbox.removeEventListener("keydown", this);
					document.body.removeChild(textbox);
				}
			});

			break;

		// shape
		default :
			img_before_shape.src = step_array[step];
			img_before_shape.onload = function (){
				ctx.globalCompositeOperation = "source-over";
				shape_start_point.x = mouse_pos.x;
				shape_start_point.y = mouse_pos.y;
				evt.preventDefault();
				canvas.addEventListener("mousemove", mouseMove, false);				
			};
			break;
	}
}

function changetool(tool){
	mode = tool;
	clear_tool(true);

	switch(mode){
		case "pencil" :
			canvas.style.cursor = "url(pencil-1.png), auto";
			document.getElementById('pencil').style.opacity = 1;
			break;
		case "eraser" :
			canvas.style.cursor = "url(eraser-1.png), auto";
			document.getElementById('eraser').style.opacity = 1;
			break;
		case "text" :
			canvas.style.cursor = "text";
			document.getElementById('text').style.opacity = 1;
			break;
		case "circle" :
			canvas.style.cursor = "url(circle-1.png), auto";
			document.getElementById('circle').style.opacity = 1;
			break;
		case "rectangle" :
			canvas.style.cursor = "url(rectangle-1.png), auto";
			document.getElementById('rectangle').style.opacity = 1;
			break;
		case "triangle" :
			canvas.style.cursor = "url(triangle-1.png), auto";
			document.getElementById('triangle').style.opacity = 1;
			break;
		default :
			canvas.style.cursor = "auto";
			break;
	}
}

// color changed
document.getElementById('color').onchange = function (){
	ctx.strokeStyle = this.value;
};

function resetcanvas(){
	clear_tool(false);

	step = -1;
	step_array = [];
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	current_img = new Image();
	push_array();
}

// push the current image to the array
function push_array(){
	step++;
	// if current step is less than the array length
	// it means that occurs undo, so need to resize the array
	if (step < step_array.length)
		step_array.length = step;
	step_array.push(canvas.toDataURL());
}

function undo(){
	clear_tool(false);
	ctx.globalCompositeOperation = "source-over";

	// console.log(step);
	if (step > 0){
		step--;
		var canvas_img = new Image();
		canvas_img.src = step_array[step];
		canvas_img.onload = function (){
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(canvas_img, 0, 0);
		};
	}
}

function redo(){
	clear_tool(false);
	ctx.globalCompositeOperation = "source-over";

	if (step < step_array.length - 1){
		step++;
		var canvas_img = new Image();
		canvas_img.src = step_array[step];
		canvas_img.onload = function (){
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(canvas_img, 0, 0);
		};
	}
}

function download(){
	clear_tool(false);
	var link = document.createElement('a');
	link.download = 'canvas.png';
	link.href = canvas.toDataURL("image/png");
	link.click();
}

// upload file changed
document.getElementById('upload').onchange = function (e){
	clear_tool(false);
	ctx.globalCompositeOperation = "source-over";
	var img = new Image();
	img.src = URL.createObjectURL(this.files[0]);

	img.onload = function (){
		// resize the image
		if (this.width > canvas.width || this.height > canvas.height){
			var img_ratio = this.height / this.width;
			var canvas_ratio = canvas.height / canvas.width;

			if (img_ratio > canvas_ratio){
				this.height = canvas.height;
				this.width = this.height / img_ratio;
			}
			else {
				this.width = canvas.width;
				this.height = this.width * img_ratio;
			}
		}

		ctx.drawImage(this, 0, 0, this.width, this.height);
		push_array();
		current_img.src = step_array[step];	
	};

	img.onerror = function (){
		alert("Image Loaded Failed");
	};

	this.value = null;
};

function clear_tool(istool){ // is pencil, eraser, text, circle, rectangle, or triangle
	var textbox;

	// if other textbox exists, draw the other textbox's value and remove other textbox
	if ((textbox = document.getElementById('textbox')) != null){
		if (textbox.value != "" && istool){
			ctx.fillText(textbox.value, last_textbox_pos.x, last_textbox_pos.y);
			push_array();
			current_img.src = step_array[step];
		}
		document.body.removeChild(textbox);
	}

	document.getElementById('pencil').style.opacity = 0.5;
	document.getElementById('eraser').style.opacity = 0.5;
	document.getElementById('text').style.opacity = 0.5;
	document.getElementById('circle').style.opacity = 0.5;
	document.getElementById('rectangle').style.opacity = 0.5;
	document.getElementById('triangle').style.opacity = 0.5;
}