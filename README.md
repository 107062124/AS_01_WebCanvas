# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.

![](readme.png)

	1. 調筆刷, 橡皮擦大小或圖形粗細的滑桿，min = 1, max = 40

	2. 調文字大小的滑桿，min = 10, max = 100

	3. 目前的字體，有6種可以選擇，分別是Arial, Comic Sans MS, Courier New, Georgia, Impact跟Times New Roman

	4. 目前的畫筆或圖形的顏色

	5. 由左至右為 :

		畫筆 - 畫畫

		橡皮擦 - 擦掉
		
		文字 - 可以在畫布任意地方點擊，點擊後會出現文字區域可以打字，打完後按Enter或點畫布其他地方文字就會出現在畫布上

	6. 由左至右為 :

		圓形 - 畫圓形
		長方形 - 畫長方形
		三角形 - 畫三角形

	7. 由左至右為 :

		上一步 - 回到上一步
		下一步 - 回到下一步
		重設畫布 - 清空畫布

	8. 由左至右為 :

		上傳圖片 - 上傳後會直接出現在畫布上，若圖片大小超出畫布會自行調整成符合畫布大小的尺寸
		下載圖片 - 下載畫布上的內容

### Function description

    Decribe your bonus function and how to use it.

### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"
[https://107062124.gitlab.io/AS_01_WebCanvas](https://107062124.gitlab.io/AS_01_WebCanvas)

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>